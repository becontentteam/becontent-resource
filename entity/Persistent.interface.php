<?php

namespace becontent\resource\entity;

interface Persistent {
	public function save();
	public function delete();
}