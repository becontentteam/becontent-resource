<?php

namespace becontent\resource\entity;

use becontent\core\control\Settings as Settings;

class Reflective {
	protected $structure;
	public function __set($name, $value) {
		$this->$name = $value;
	}
	public function __get($name) {
		return $this->$name;
	}
	
	/**
	 * Constructor to create the Reflective class must be called at the end of every constructor
	 * of classes that inherit from Reflective
	 */
	public function __construct() {
		if (Settings::getOperativeMode () == "debug")
			echo "<br> reflecting on properties";
		$this->structure = $this->reflectProperties ();
	}
	public function reflectProperties() {
		
		/**
		 * Php's ReflectionClass
		 */
		$reflector = new \ReflectionClass ( $this );
		/**
		 * Acquiring properties
		 */
		$properties = $reflector->getProperties ();
		$decodedProperties = null;
		
		/**
		 * Iterating over properties
		 */
		foreach ( $properties as $k => $v ) {
			
			$type = "string";
			$multiplicity = "0,1";
			$name = $v->getName ();
			
			/**
			 * Read annotations
			 */
			$annotation = $v->getDocComment ();
			
			/**
			 */
			if (! preg_match ( '/@volatile/', $annotation )) {
				if ($name != "persistenceMediator" && $name != "structure") {
					
					/**
					 * Attribute in sight
					 */
					if (preg_match ( '/@var/', $annotation )) {
						
						/**
						 * in this case an annotation has been specified to describe an attribute
						 */
						$decodedProperties [$name] = $this->decodeField ( $annotation, $name );
					} else /**
					 * Relation in sight
					 */
					if (preg_match ( '/@rel/', $annotation )) {
						
						/**
						 * in this case an annotation has been specified to describe a relation
						 */
						$decodedProperties [$name] = $this->decodeRelation ( $annotation, $name );
						
						/**
						 * In case of multiplicity different from One the attribute must be initialized with an empty array
						 */
						if (! isset ( $this->$name ))
							$this->$name = array ();
					}
					
					if (! isset ( $decodedProperties [$name] )) {
						/**
						 * in case of no annotation or bad format annotation a default-type attribute is created
						 */
						$decodedProperties [$name] = array (
								"name" => $name,
								"type" => $type 
						);
					} else if ($decodedProperties [$name] == null) {
						/**
						 * in case of no annotation or bad format annotation a default-type attribute is created
						 */
						$decodedProperties [$name] = array (
								"name" => $name,
								"type" => $type 
						);
					}
					
					/**
					 * Prevent from creation of inherited properties
					 */
					if ($reflector->getParentClass ()->hasProperty ( $name )) {
						$decodedProperties [$name] ["inherited"] = true;
					} else {
						$decodedProperties [$name] ["inherited"] = false;
					}
				}
			}
		}
		return $decodedProperties;
	}
	
	/**
	 *
	 * @param unknown $annotation        	
	 * @return multitype:unknown
	 */
	private function decodeField($annotation, $name) {
		$decodedField = null;
		
		$lines = explode ( "@", $annotation );
		$type = "string"; // The default type;
		foreach ( $lines as $k => $v ) {
			$annotation = explode ( " ", $v );
			for($i = 0; $i < count ( $annotation ); $i ++) {
				if ($annotation [$i] == "var") {
					$type = preg_replace ( '/\s+/', '', $annotation [$i + 1] );
				}
			}
			$decodedField = array (
					"name" => $name,
					"type" => $type 
			);
		}
		return $decodedField;
	}
	/**
	 *
	 * @param unknown $annotation        	
	 * @return Ambigous <unknown, multitype:unknown >
	 */
	private function decodeRelation($annotation, $name) {
		$decodedRelation = null;
		
		$lines = explode ( "@", $annotation );
		
		foreach ( $lines as $k => $v ) {
			$annotation = explode ( " ", $v );
			for($i = 0; $i < count ( $annotation ); $i ++) {
				if ($annotation [$i] == "rel") {
					$class = preg_replace ( '/\s+/', '', $annotation [$i + 1] );
				}
				if ($annotation [$i] == "multiplicity") {
					$multiplicity = preg_replace ( '/\s+/', "", $annotation [$i + 1] );
				}
				if ($annotation [$i] == "inversedBy") {
					$inversedBy = preg_replace ( '/\s+/', "", $annotation [$i + 1] );
				}
				if ($annotation [$i] == "mappedBy") {
					$mappedBy = preg_replace ( '/\s+/', "", $annotation [$i + 1] );
				}
			}
		}
		$decodedRelation = array (
				"name" => $name,
				"class" => $class,
				"multiplicity" => $multiplicity 
		);
		if (isset ( $inversedBy )) {
			$decodedRelation ['inversedBy'] = $inversedBy;
		}
		if (isset ( $mappedBy )) {
			$decodedRelation ['mappedBy'] = $mappedBy;
		}
		return $decodedRelation;
	}
	/**
	 */
	public function reflectMethods() {
	}
}
?>