<?php

namespace becontent\resource\entity;

use becontent\core\foundation\DB as DB;
use becontent\beContent as beContent;
use becontent\meta_model\mediator\Entity as Entity;
use becontent\resource\foundation\DoctrinePersistentManager as DoctrinePersistentManager;
use becontent\core\control\Settings as Settings;

class Resource extends Reflective implements Persistent, \JsonSerializable {
	/**
	 * Unique resource identifier
	 *
	 * @var integer
	 */
	protected $id;
	protected $resDepth = 0;
	
	/**
	 */
	public function __construct() {
		$this->persistenceManager = beContent::getInstance ()->getPersistenceManager ();
		
		if (Settings::getModMode () == true) {
			$this->analize ();
			$this->persistenceManager->registerResource ( $this->structure, get_class ( $this ) );
		}
	}
	
	/**
	 * Calls reflective constructor to analize structure
	 */
	public function analize() {
		parent::__construct ();
	}
	
	/*
	 *
	 */
	public function save() {
		if (! isset ( $this->persistenceManager ))
			$this->persistenceManager = beContent::getInstance ()->getPersistenceManager ();
		
		$this->persistenceManager->saveResource ( $this );
		
		$time = round ( microtime ( true ) * 1000 );
		file_put_contents ( Settings::getContentsPath () . '/doctrine/logs/' . preg_replace ( "/\\\\/", "_", get_class ( $this ) ), $time );
	}
	/**
	 */
	public function delete() {
		if (! isset ( $this->persistenceManager ))
			$this->persistenceManager = beContent::getInstance ()->getPersistenceManager ();
		
		$this->persistenceManager->deleteResource ( $this );
		$time = round ( microtime ( true ) * 1000 );
		file_put_contents ( Settings::getContentsPath () . '/doctrine/logs/' . preg_replace ( "/\\/", "_", get_class ( $this ) ), $time );
	}
	public function getUnderscoreResourceClassifier() {
		return preg_replace ( "/\\/", "_", get_class ( $this ) );
	}
	
	/**
	 *
	 * @return \becontent\resource\entity\Resource
	 */
	public function jsonSerialize() {
		if (! isset ( $this->structure ))
			$this->analize ();
		
		$jsonVersion = new \stdClass ();
		$jsonVersion->resourceClassifier = preg_replace ( "/doctrine\\\\proxies\\\\__CG__\\\\/", "", get_class ( $this ) );
		$jsonVersion->resourceIdentifier = $this->id;
		foreach ( $this->structure as $k => $v ) {
			
			$attrName = $v ['name'];
			if ($v ['multiplicity'] == "OneToMany") {
				$jsonVersion->{$attrName} = array ();
				
				if (isset ( $this->{$attrName} )) {
					/**
					 * Triggers lazy initialization of elements
					 */
					if (is_object ( $this->{$attrName} ))
						$values = $this->{$attrName}->getValues ();
					else {
						$values = $this->{$attrName};
					}
					$i = 0;
					foreach ( $values as $innerKey => $innerValue ) {
						if ($this->resDepth == 0) {
							$jsonVersion->{$attrName} [$i] = new \stdClass ();
							$jsonVersion->{$attrName} [$i]->resourceIdentifier = $innerValue->id;
							$jsonVersion->{$attrName} [$i]->resourceClassifier = $v ["class"];
							$jsonVersion->{$attrName} [$i]->isInitialized = false;
						} else {
							$innerValue->resDepth = $this->resDepth - 1;
							$jsonVersion->{$attrName} [$i] = json_decode ( json_encode ( $innerValue ) );
						}
						$i ++;
					}
				}
			} else {
				if (isset ( $v ["class"] ) && isset ( $this->$attrName )) {
					$className = $v ["class"];
					if (get_class ( $this->$attrName ) != $v ["class"]) {
						/**
						 * Doctrine fix!!!
						 */
						/*
						 * It means that the object hasn't been loaded in the meanwhile so the lazy initialization has to be triggered manually
						 */
						if (method_exists ( $this->$attrName, "__load" )) {
							$this->$attrName->__load ();
							$className = preg_replace ( "/doctrine\\\\proxies\\\\__CG__\\\\/", "", get_class ( $this->$attrName ) );
						}
					}
					if ($this->resDepth == 0) {
						$jsonVersion->{$attrName} = new \stdClass ();
						$jsonVersion->{$attrName}->resourceIdentifier = $this->$attrName->id;
						$jsonVersion->{$attrName}->resourceClassifier = $className;
						$jsonVersion->{$attrName}->isInitialized = false;
					} else {
						
						$this->$attrName->resDepth = $this->resDepth - 1;
						$jsonVersion->{$attrName} = json_decode ( json_encode ( $this->$attrName ) );
					}
				} else {
					$jsonVersion->$attrName = $this->__get ( $attrName );
				}
			}
		}
		return $jsonVersion;
	}
}
?>