<?php

namespace becontent\resource\control;

use becontent\beContent as beContent;
use Doctrine\Common\Collections\ArrayCollection as ArrayCollection;

class ResourceEditor {
	private $fetchedResources;
	
	/**
	 * This method is able to save every kind of resource taking the data represented in json string
	 *
	 * @param unknown $resourceClassifier        	
	 * @param unknown $newResourceAsJson        	
	 */
	public function addResource($resourceClassifier, $newResourceAsJsonObj) {
		$newResource = new $resourceClassifier ();
		
		$newResource->save ();
		
		$newResource = $this->setAttributesValues ( $newResource, $newResourceAsJsonObj );
		
		$newResource->save ();
		
		beContent::getInstance ()->getPersistenceManager ()->flush ();
		
		$this->fetchedResources [$resourceClassifier] [$newResource->id] = $newResource;
		
		return $newResource;
	}
	
	/**
	 *
	 * @param Resource $resourceToEdit        	
	 * @param mixed $incomingData        	
	 */
	private function setAttributesValues($resourceToEdit, $incomingData) {
		foreach ( $incomingData as $key => $value ) {
			
			if (property_exists ( $resourceToEdit, $key )) {
				$resourceToEdit->{$key} = $value;
			}
		}
		return $resourceToEdit;
	}
	
	/**
	 * This method is able to edit every kind of resource taking the data represented in json string
	 *
	 * @param unknown $resourceClassifier        	
	 * @param unknown $resourceIdentifier        	
	 * @param unknown $resourceToEditAsJson        	
	 */
	public function editResource($resourceClassifier, $resourceIdentifier, $resourceToEditAsJsonObj) {
		
		$resourceToEdit = becontent::getInstance ()->resource ( $resourceClassifier, $resourceIdentifier );
		
		$this->setAttributesValues ( $resourceToEdit, $resourceToEditAsJsonObj );
		
		$resourceToEdit->save ();
		
		return $resourceToEdit;
	}
	
	/**
	 *
	 * @param unknown $masterResourceClassifier        	
	 * @param unknown $masterResourceIdentifier        	
	 * @param unknown $linkProperty        	
	 * @param unknown $slaveResourceClassifier        	
	 * @param unknown $slaveResourceIdentifier        	
	 */
	public function linkResources($masterResourceClassifier, $masterResourceIdentifier, $linkProperty, $slaveResourceClassifier, $slaveResourceIdentifier) {
		/**
		 */
		$success = false;
		
		/**
		 * Retrieving $masterResource and $slaveResource if they're not available in fetched resources, otherwise these resources are loaded from the $this->fetchedResourcesMap
		 */
		$masterResource = becontent::getInstance ()->resource ( $masterResourceClassifier, $masterResourceIdentifier );
		
		/**
		 */
		$slaveResource = becontent::getInstance ()->resource ( $slaveResourceClassifier, $slaveResourceIdentifier );
		
		/**
		 * initializing a $saveNeeded check
		 */
		$saveNeeded = false;
		
		/**
		 */
		if (isset ( $masterResource ) && isset ( $slaveResource )) {
			
			if (property_exists ( $masterResource, $linkProperty )) {
				
				if (! isset ( $masterResource->structure ))
					$masterResource->analize ();
				
				if ($masterResource->structure [$linkProperty] ["multiplicity"] == "ManyToMany") {
					
					if (isset ( $masterResource->structure [$linkProperty] ["mappedBy"] )) {
						/**
						 */
						$mappedProperty = $masterResource->structure [$linkProperty] ["mappedBy"];
						$slaveResource->{$mappedProperty} [] = $masterResource;
					} else {
						/**
						 */
						$masterResource->{$linkProperty} [] = $slaveResource;
					}
				} else if ($masterResource->structure [$linkProperty] ["multiplicity"] == "OneToMany") {
					if (isset ( $masterResource->structure [$linkProperty] ["mappedBy"] )) {
						/**
						 */
						$mappedProperty = $masterResource->structure [$linkProperty] ["mappedBy"];
						$slaveResource->$mappedProperty = $masterResource;
					} else {
						/**
						 */
						$masterResource->$linkProperty = $slaveResource;
					}
				} else {
					/**
					 * In this case we're in a simple OneToX relation that hasn't to be mantained (it involves inversed directive that responds correctly to cascading options)
					 */
					$masterResource->{$linkProperty} = $slaveResource;
				}
				
				$saveNeeded = true;
				
				$success = true;
			}
		} else {
			throw new \Exception ( "Requested resources couldn't be found" );
		}
		
		/**
		 * Persisting (but not flushing) all operations 'till now)
		 */
		if ($saveNeeded) {
			$slaveResource->save ();
			$masterResource->save ();
		}
		
		return $success;
	}
	
	/**
	 *
	 * @param unknown $masterResourceClassifier        	
	 * @param unknown $masterResourceIdentifier        	
	 * @param unknown $linkProperty        	
	 * @param unknown $slaveResourceClassifier        	
	 * @param unknown $slaveResourceIdentifier        	
	 */
	public function unlinkResources($masterResourceClassifier, $masterResourceIdentifier, $linkProperty, $slaveResourceClassifier, $slaveResourceIdentifier) {
		
		/**
		 * initialize a $saveNeeded check
		 */
		$saveNeeded = false;
		/**
		 * initialize a $success check
		 */
		$success = false;
		
		/**
		 * Retrieve $masterResource
		 */
		if (! isset ( $this->fetchedResources [$masterResourceClassifier] [$masterResourceIdentifier] )) {
			$masterResource = becontent::getInstance ()->resource ( $masterResourceClassifier, $masterResourceIdentifier );
			$this->fetchedResources [$masterResourceClassifier] [$masterResourceIdentifier] = $masterResource;
		} else {
			$masterResource = $this->fetchedResources [$masterResourceClassifier] [$masterResourceIdentifier];
		}
		
		/**
		 * Check if required property exists in the masterResource
		 */
		if (property_exists ( $masterResource, $linkProperty )) {
			
			if (! isset ( $masterResource->structure ))
				$masterResource->analize ();
			
			/**
			 * This part has to be better engineered, maybe a "relation-policy-manager" has to be built,
			 * for now the following code is responsible to handle the operations on relationships realized through collections (0,...* to 0,...*) (0,1 to 0,...*)
			 */
			if ($masterResource->structure [$linkProperty] ["multiplicity"] == "OneToMany" || $masterResource->structure [$linkProperty] ["multiplicity"] == "ManyToMany") {
				
				/*
				 * If slaveResourceIdentifier is defined
				 */
				if ($slaveResourceIdentifier != 0 && $slaveResourceIdentifier != "") {
					/**
					 * Retrieve the slaveResource
					 */
					if (! isset ( $this->fetchedResources [$slaveResourceClassifier] [$slaveResourceIdentifier] )) {
						$slaveResource = becontent::getInstance ()->resource ( $slaveResourceClassifier, $slaveResourceIdentifier );
						$this->fetchedResources [$slaveResourceClassifier] [$slaveResourceIdentifier] = $slaveResource;
					} else {
						$slaveResource = $this->fetchedResources [$slaveResourceClassifier] [$slaveResourceIdentifier];
					}
					
					/**
					 * The removal operation is postponed in order to handle cascading before the operation
					 */
					$masterResource->{$linkProperty}->remove ( $slaveResource );
					
					/**
					 * Mapped relationships
					 * the mappedBy directive isn't subject of cascading options, the user has the responsibility to mantain mapping
					 * for what mapping means and how to understand mappedBy and inversedBy directives see documentation on resource interface
					 */
					if (isset ( $masterResource->structure [$linkProperty] ["mappedBy"] )) {
						
						/**
						 * put in $mappedProperty the name of the property owned by slaveResource that maps the relation represented in masterResource through $linkProperty
						 */
						$mappedProperty = $masterResource->structure [$linkProperty] ["mappedBy"];
						
						/**
						 * The mapping property could be a ManyToMany property itself ( is the case of a ManyToMany bidirectional relation )
						 */
						if ($masterResource->structure [$mappedProperty] ["multiplicity"] == "ManyToMany") {
							
							/**
							 * In this case $masterResource colud already has been inserted in the collection.
							 * To avoid duplicates verify this situation
							 */
							if (! $slaveResource->{$mappedProperty}->contains ( $masterResource ) || $masterResource->structure [$linkProperty] ["multiplicity"] == "OneToMany")
								$slaveResource->{$mappedProperty}->remove ( $masterResource );
						} else
							/**
							 * In this case we are in a ManyToOne bidirectional relation so the mapping property is just a OneToMany
							 */
							$slaveResource->{$mappedProperty} = null;
					}
				}
			} else {
				/**
				 * In any case imposing null to a ManyToOne or to a OneToOne causes the triggering of the cascanding policies
				 */
				$masterResource->{$linkProperty} = null;
			}
			
			$saveNeeded = true;
			
			$success = true;
		} else
			throw new \Exception ( "Requested resources couldn't be found" );
		
		/**
		 * Persisting (but not flushing) all operations 'till now)
		 */
		if ($saveNeeded) {
			if (isset ( $slaveResource ))
				$slaveResource->save ();
			$masterResource->save ();
		}
		
		return $success;
	}
	
	/**
	 *
	 * @param unknown $resourceClassifier        	
	 * @param unknown $resourceIdentifier        	
	 */
	public function deleteResource($resourceClassifier, $resourceIdentifier) {
		$success = false;
		
		$resourceToDelete = becontent::getInstance ()->resource ( $resourceClassifier, $resourceIdentifier );
		$success = $resourceToDelete->delete ();
		
		return $success;
	}
}
?>