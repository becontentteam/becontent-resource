<?php

namespace becontent\resource\foundation;

interface PersistentManager {
	function initialize();
	function loadResource($resourceClassifier, $resourceInstanceIdentifier);
	function saveResource($resource);
	function deleteResource($resource);
	function flush();
}