<?php

namespace becontent\resource\foundation;

use becontent\core\foundation\DB as DB;
use becontent\core\control\Settings as Settings;
use becontent\resource\foundation\PersistentManager;
use Doctrine\ORM\Tools\Setup as Setup;
use Doctrine\ORM\EntityManager as EntityManager;
use becontent\core\control\Config as Config;

/**
 *
 * @author Nicola
 *        
 */
class DoctrinePersistentManager implements PersistentManager {
	/**
	 *
	 * @var unknown
	 */
	private $entityManager;
	/**
	 *
	 * @var unknown
	 */
	private $dbParams;
	/**
	 *
	 * @var unknown
	 */
	private $config;
	/**
	 *
	 * @var unknown
	 */
	private $isDevMode;
	function __construct() {
		$this->initialize ();
	}
	/**
	 * (non-PHPdoc)
	 *
	 * @see \becontent\resource\foundation\PersistentManager::initialize()
	 */
	public function initialize() {
		$this->isDevMode = false;
		
		// the connection configuration
		$this->dbParams = array (
				'driver' => 'pdo_mysql',
				'user' => Config::getInstance ()->getConfigurations ()['database']['username'],
				'password' => Config::getInstance ()->getConfigurations ()['database']['password'],
				'dbname' => Config::getInstance ()->getConfigurations ()['database']['database'],
				'host' => Config::getInstance ()->getConfigurations ()['database']['host'] 
		);
		
		$this->driver = new \Doctrine\ORM\Mapping\Driver\XmlDriver ( array (
				Settings::getContentsPath () . "/doctrine/mappings" 
		) );
		
		if (Settings::getOperativeMode () == "debug")
			$applicationMode = "development";
		else
			$applicationMode = "deploy";
		
		if ($applicationMode == "development") {
			$cache = new \Doctrine\Common\Cache\ArrayCache ();
		} else {
			$cache = new \Doctrine\Common\Cache\ApcCache ();
		}
		
		$this->paths = array (
				Settings::getContentsPath () . "/doctrine/mappings" 
		);
		
		$this->config = Setup::createAnnotationMetadataConfiguration ( $this->paths, $this->isDevMode );
		$this->config->setMetadataDriverImpl ( $this->driver );
		$this->config->setQueryCacheImpl ( $cache );
		$this->config->setProxyDir ( Settings::getContentsPath () . '/doctrine/proxies' );
		$this->config->setProxyNamespace ( 'doctrine\proxies' );
		
		$this->config->setAutoGenerateProxyClasses ( true );
		
		$this->entityManager = EntityManager::create ( $this->dbParams, $this->config );
	}
	/**
	 *
	 * @param unknown $resourceStructure        	
	 * @param unknown $resourceClassifier        	
	 */
	function registerResource($resourceStructure, $resourceClassifier) {
		$this->structureToXML ( $resourceStructure, $resourceClassifier );
	}
	
	
	function persistSchema() {
		$schemaManager = $this->entityManager->getConnection ()->getSchemaManager ();
		$classes = $this->entityManager->getMetadataFactory ()->getAllMetadata ();
		foreach ( $classes as $k => $v ) {
			if ($schemaManager->tablesExist ( array (
					$v->table ["name"] 
			) ) == false) {
				$schemaTool = new \Doctrine\ORM\Tools\SchemaTool ( $this->entityManager );
				$schemaTool->createSchema ( array (
						$v 
				) );
			}
		}
	}
	
	/**
	 *
	 * @param unknown $resourceStructure        	
	 * @param unknown $resourceClassifier        	
	 */
	public function structureToXML($resourceStructure, $resourceClassifier) {
		if (! file_exists ( Settings::getContentsPath () . "/doctrine/mappings/" . str_replace ( "\\", ".", $resourceClassifier ) . ".dcm.xml.lock" )) {
			
			/**
			 * Blocking auto generation Loop *
			 */
			file_put_contents ( Settings::getContentsPath () . "/doctrine/mappings/" . str_replace ( "\\", ".", $resourceClassifier ) . ".dcm.xml.lock", date ( 'm/d/Y h:i:s a', time () ) );
			
			$xml = new \SimpleXMLElement ( '<doctrine-mapping/>' );
			
			$entity = $xml->addChild ( 'entity' );
			$entity->addAttribute ( "name", $resourceClassifier );
			$entity->addAttribute ( "table", str_replace ( "\\", "_", $resourceClassifier ) );
			$autoId = true;
			foreach ( $resourceStructure as $k => $v ) {
				
				if ($v ["inherited"] == true) {
					if (isset ( $v ["class"] )) {
						if (! file_exists ( Settings::getContentsPath () . "/doctrine/mappings/" . str_replace ( "\\", ".", $v ["class"] ) . ".dcm.xml.lock" )) {
							/**
							 * Automatic creation of nested files
							 */
							$involvedPrototype = new $v ["class"] ();
							$involvedPrototype->analize ();
							$this->structureToXML ( $involvedPrototype->structure, $v ["class"] );
						}
					}
				} else {
					if (isset ( $v ["type"] )) {
						
						if ($v ["name"] === "id") {
						
						/**
						 * Ignore this field it has been created in resource superclass
						 */
						} else {
							$field = $this->encodeAttributeToXml ( $v, $entity );
						}
					} else {
						$relation = $this->encodeRelationToXml ( $v, $entity,$resourceClassifier);
					}
				}
			}
			
			echo "<br> generating" . Settings::getContentsPath () . "/doctrine/mappings/" . str_replace ( "\\", ".", $resourceClassifier ) . ".dcm.xml";
			
			/*
			 * if ($autoId) { $field = $this->autoPrimaryKeyGenerator ( $entity ); }
			 */
			$xml->asXML ( Settings::getContentsPath () . "/doctrine/mappings/" . str_replace ( "\\", ".", $resourceClassifier ) . ".dcm.xml" );
		}
	}
	
	/**
	 *
	 * @param unknown $entityAsXml        	
	 * @return unknown
	 */
	private function autoPrimaryKeyGenerator($entityAsXml) {
		$field = $entityAsXml->addChild ( 'id' );
		$field->addAttribute ( "name", "id" );
		$field->addAttribute ( "type", "integer" );
		$generator = $field->addChild ( "generator" );
		$generator->addAttribute ( "strategy", "AUTO" );
		return $field;
	}
	/**
	 *
	 * @param unknown $primaryKeyStructure        	
	 * @param unknown $entityAsXml        	
	 */
	private function encodePrimaryKeyToXml($primaryKeyStructure, $entityAsXml) {
		$field = $entityAsXml->addChild ( 'id' );
		$generator = $field->addChild ( "generator" );
		$generator->addAttribute ( "strategy", "AUTO" );
		$field->addAttribute ( "name", $primaryKeyStructure ["name"] );
		
		if ($primaryKeyStructure ["type"] == "NULL") {
			$primaryKeyStructure ["type"] = "string";
		}
		$field->addAttribute ( "type", $primaryKeyStructure ["type"] );
		return $field;
	}
	/**
	 *
	 * @param unknown $attributeStructure        	
	 * @param unknown $entityAsXml        	
	 */
	private function encodeAttributeToXml($attributeStructure, $entityAsXml) {
		$field = $entityAsXml->addChild ( 'field' );
		$field->addAttribute ( "name", $attributeStructure ["name"] );
		
		if ($attributeStructure ["type"] == "NULL") {
			$attributeStructure ["type"] = "string";
		}
		$field->addAttribute ( "type", $attributeStructure ["type"] );
		return $field;
	}
	/**
	 *
	 * @param unknown $relationStructure        	
	 * @param unknown $entityAsXml        	
	 */
	private function encodeRelationToXml($relationStructure, $entityAsXml,$resourceClassifier) {
		$field = null;
		if ($relationStructure ["multiplicity"] === "ManyToOne") {
			$field = $entityAsXml->addChild ( 'many-to-one' );
		} else if ($relationStructure ["multiplicity"] === "OneToMany") {
			$field = $entityAsXml->addChild ( 'one-to-many' );
		} else if ($relationStructure ["multiplicity"] === "OneToOne") {
			$field = $entityAsXml->addChild ( 'one-to-one' );
			
		} else if ($relationStructure ["multiplicity"] === "ManyToMany") {
			$field = $entityAsXml->addChild ( 'many-to-many' );
		}
		
		$field->addAttribute ( 'field', $relationStructure ['name'] );
		$field->addAttribute ( 'target-entity', $relationStructure ["class"] );
		if (isset ( $relationStructure ['inversedBy'] ))
			$field->addAttribute ( 'inversed-by', $relationStructure ["inversedBy"] );
		if (isset ( $relationStructure ['mappedBy'] ))
			$field->addAttribute ( 'mapped-by', $relationStructure ["mappedBy"] );
		
		$cascadePolicy = $field->addChild ( "cascade" );
		$cascadePolicy->addChild ( "cascade-all" );
		
		if (! file_exists ( Settings::getContentsPath () . "/doctrine/mappings/" . str_replace ( "\\", ".", $relationStructure ["class"] ) . ".dcm.xml.lock" )) {
			/**
			 * Automatic creation of nested files
			 */
			$involvedPrototype = new $relationStructure ["class"] ();
			$involvedPrototype->analize ();
			$this->structureToXML ( $involvedPrototype->structure, $relationStructure ["class"] );
		}
		
		return $field;
	}
	
	/**
	 * (non-PHPdoc)
	 *
	 * @see \becontent\resource\foundation\PersistentManager::loadResource()
	 */
	function loadResource($resourceClassifier, $resourceInstanceIdentifier) {
		return $this->entityManager->find ( $resourceClassifier, $resourceInstanceIdentifier );
	}
	
	/**
	 * (non-PHPdoc)
	 *
	 * @see \becontent\resource\foundation\PersistentManager::loadResource()
	 */
	function findResources($resourceClassifier, $resourceCriteria = null, $orderCriteria = null) {
		return $this->entityManager->getRepository ( $resourceClassifier )->findBy ( $resourceCriteria, $orderCriteria );
	}
	
	/**
	 * (non-PHPdoc)
	 *
	 * @see \becontent\resource\foundation\PersistentManager::saveResource()
	 */
	function saveResource($resource) {
		$this->entityManager->persist ( $resource );
	}
	/**
	 * (non-PHPdoc)
	 *
	 * @see \becontent\resource\foundation\PersistentManager::deleteResource()
	 */
	function deleteResource($resource) {
		$this->entityManager->remove ( $resource );
	}
	/**
	 * (non-PHPdoc)
	 *
	 * @see \becontent\resource\foundation\PersistentManager::flush()
	 */
	function flush() {
		$this->entityManager->flush ();
	}
	function rollback() {
		$this->entityManager->rollback ();
	}
	function commit() {
		$this->entityManager->commit ();
	}
	function beginTransaction() {
		$this->entityManager->beginTransaction ();
	}
}